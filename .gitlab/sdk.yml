variables:
  CI_DESIGNATED_BRANCH: prplos

stages:
  - generate
  - build

.build feed with SDK:
  tags:
    - package-builder
  stage: build
  image: $CI_SDK_IMAGE
  variables:
    CI_SDK_TOPDIR: /home/builder
    CI_SDK_INSTALL_FEEDS: base packages luci routing
    CI_SDK_BUILD_CONFIG: >
      +BUILD_LOG -AUTOREMOVE -ALL -ALL_NONSHARED -ALL_KMODS -SIGNED_PACKAGES

  before_script:
    - set -o pipefail
    - git config --global --add safe.directory '*'

    - cd $CI_SDK_TOPDIR
    - mkdir logs

    - sed -i -E 's;git.openwrt.org/(feed|project);github.com/openwrt;' feeds.conf.default
    - echo "src-include defaults feeds.conf.default" > feeds.conf
    - echo "src-link ci $CI_PROJECT_DIR" >> feeds.conf

    - >
      for feed in $CI_SDK_INSTALL_FEEDS ci; do
        ./scripts/feeds update "$feed" ;
        ./scripts/feeds install -a -p "$feed" ;
      done 2>&1 | tee logs/build.log

    - sync
    - sleep 1
    - sync

    - >
      for option in $CI_SDK_BUILD_CONFIG $CI_SDK_BUILD_CONFIG_EXTRA; do
        echo "$option" | sed -E "s/^\+(.*)$/CONFIG_\1=y/;s/^\-(.*)$/CONFIG_\1=n/" >> .config
      done
    - cat .config | tee --append logs/build.log
    - sed -i '/^config SAH_/{N;/\n\tbool/N;/\n\tdefault/d;}' Config-build.in
    - make defconfig | tee --append logs/build.log
    - ./scripts/diffconfig.sh | tee --append logs/build.log

    - sync
    - sleep 1
    - sync

  script:
    - git config --global --add safe.directory '*'

    - >
      test -n "$CI_SDK_BEFORE_COMPILE_COMMAND" &&
        echo "Running CI_SDK_BEFORE_COMPILE_COMMAND=$CI_SDK_BEFORE_COMPILE_COMMAND" &&
        /bin/sh -c "$CI_SDK_BEFORE_COMPILE_COMMAND"

    - >
      for package in $CI_SDK_BUILD_PACKAGES; do
        make V=sc package/$package/{download,check} FIXUP=1
      done 2>&1 | tee --append logs/build.log

    - >
      topdir=$(pwd);
      for feed in $(find feeds -follow -name .git); do
        pushd $(dirname $feed) > /dev/null; git diff-index --exit-code HEAD || {
          ret=$?
          echo "Feed $(dirname $feed) packages integrity issues, please check feed-packages-hash-issues.patch from artifacts"
          git diff | tee $topdir/feed-packages-hash-issues.patch
          exit $ret
        }
        popd > /dev/null
      done

    - >
      for package in $CI_SDK_BUILD_PACKAGES; do
        make -j ${CI_SDK_BUILD_PARALLEL:-$(nproc)} package/$package/compile
      done 2>&1 | tee --append logs/build.log

  after_script:
    - cp -R "$CI_SDK_TOPDIR/logs" "$CI_SDK_TOPDIR/bin" $CI_PROJECT_DIR || true
    - >
      if grep -qr 'make\[[[:digit:]]\].*Error [[:digit:]]$' logs; then
        printf "\n====== Showing Make errors found in the log files ======";
        for file in $(grep -lr 'make\[[[:digit:]]\].*Error [[:digit:]]$' logs); do
          printf "\n====== Make errors from $CI_JOB_URL/artifacts/file/$file ======\n" ;
          grep -r -C5 'make\[[[:digit:]]\].*Error [[:digit:]]$' $file ;
        done
      fi

  artifacts:
    expire_in: 1 month
    when: always
    paths:
      - logs/
      - ./feed-packages-hash-issues.patch

.generate SDK package build jobs:
  stage: generate
  image: alpine

  variables:
    CI_SDK_BUILD_PACKAGES: |
      please-provide
    CI_SDK_PACKAGE_JOBS_TEMPLATE: |
      {% if env['CI_SDK_PIPELINE_SOURCE'] %}
      workflow:
        rules:
          - if: $$CI_PIPELINE_SOURCE == "{{ env['CI_SDK_PIPELINE_SOURCE'] }}"
      {% endif %}
      include:
        - remote: https://gitlab.com/prpl-foundation/prplos/prplos/-/raw/$CI_DESIGNATED_BRANCH/.gitlab/sdk.yml
      {% for sdk in env['CI_SDK_TARGETS'].rstrip().split("\n") %}
        {% for package in env['CI_SDK_BUILD_PACKAGES'].rstrip().split("\n") %}
      build {{ package | trim }} with {{ sdk | trim }} SDK:
        extends: .build feed with {{ sdk | trim }} SDK
        variables:
          CI_SDK_BUILD_PACKAGES: {{ package | trim }}
          CI_SDK_BUILD_CONFIG_EXTRA: +PACKAGE_{{ package | trim }}
        {% endfor %}
      {% endfor %}

  before_script:
    - apk add python3 py3-pip
    - pip3 install jinja2

  script:
    - |
      echo "$CI_SDK_PACKAGE_JOBS_TEMPLATE" | python3 -c '
      import os
      import sys
      import jinja2
      sys.stdout.write(
        jinja2.Template(sys.stdin.read()
      ).render(env=os.environ))' > sdk-package-jobs.yml
    - cat sdk-package-jobs.yml

  artifacts:
    paths:
      - sdk-package-jobs.yml

.execute SDK package build jobs:
  stage: build
  needs:
    - generate

  trigger:
    include:
      - artifact: sdk-package-jobs.yml
        job: generate
    strategy: depend

.build feed with OpenWrt SDK:
  extends: .build feed with SDK
  variables:
    CI_SDK_BEFORE_COMPILE_COMMAND: sudo apt-get update; sudo apt-get install -y python-yaml python3-yaml
    CI_SDK_TOPDIR: /home/build/openwrt

.build feed with intel_mips-xrx500 SDK:
  extends: .build feed with SDK
  variables:
    CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/$CI_DESIGNATED_BRANCH/sdk-intel_mips-xrx500:latest
    CI_SDK_INSTALL_FEEDS: base packages luci routing feed_intel

.build feed with ipq40xx-generic SDK:
  extends: .build feed with SDK
  variables:
    CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/$CI_DESIGNATED_BRANCH/sdk-ipq40xx-generic:latest

.build feed with mvebu-cortexa9 SDK:
  extends: .build feed with SDK
  variables:
    CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/$CI_DESIGNATED_BRANCH/sdk-mvebu-cortexa9:latest

.build feed with x86-64 SDK:
  extends: .build feed with SDK
  variables:
    CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/$CI_DESIGNATED_BRANCH/sdk-x86-64:latest

.build feed with ipq807x-generic SDK:
  extends: .build feed with SDK
  variables:
    CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/$CI_DESIGNATED_BRANCH/sdk-ipq807x-generic:latest

.build feed with ath79-generic-19.07.7 SDK:
  extends: .build feed with OpenWrt SDK
  variables:
    CI_SDK_IMAGE: openwrtorg/sdk:ath79-generic-19.07.7

.build feed with x86-64-prplos-next SDK:
   extends: .build feed with SDK
   variables:
     CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/prplos-next/sdk-x86-64:latest

.build feed with ipq807x-generic-prplos-next SDK:
   extends: .build feed with SDK
   variables:
     CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/prplos-next/sdk-ipq807x-generic:latest

.build feed with mvebu-cortexa9-prplos-next SDK:
   extends: .build feed with SDK
   variables:
     CI_SDK_IMAGE: registry.gitlab.com/prpl-foundation/prplos/prplos/prplos-next/sdk-mvebu-cortexa9:latest
